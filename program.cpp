#include "shapes.h"
#include <iostream>
#include <vector>

using namespace std;

int main(){

  vector <Shape*> shapes;
  shapes.push_back(new Circle(10.0));
  shapes.push_back(new Square(5.0));
  shapes.push_back(new Rectangle(4.0,6.7));
  for(unsigned i = 0;i<shapes.size();++i)
    cout << "area of shape " << i+1
	 << " is " << shapes[i]->area() << endl;

  for(Shape* s : shapes)
    delete s;
  shapes.clear();

}
