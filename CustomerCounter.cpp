#include "CustomerCounter.h"
#include <string>

CustomerCounter:CustomerCounter(int maxNumber)
{
  this->maximum_customers = maxNumber;
}

CustomerCounter:~CustomerCounter()
{
   
}

std::string CustomerCounter:add(int number)
{
  if(customer_count+number<=maximum_customers){
    this->customer_count+= number;
    return "customer count was updated succesfully";
  }
  else{
    return "customer count cannot exceed the maximum customer number";
  }
}

std::string CustomerCounter:substract(int number)
{
  if(this->customer_count-number>=0)
    {
      this->customer_count-= number;
      return "customer count was updated succesfully";
    }
  else
    {
      return "customer count cannot be less than 0";
  }
}  
