#ifndef _PERSON_H_
#define _PERSON_H_

#include <string>
#include <vector>

class Person {
 private:
  std::string name;
  std::vector<Person*> friends;
 public:
  Person(std::string name);
  ~Person();
  std::string befriend(Person friend);
  std::string unfriend(Person friend);

  
};
