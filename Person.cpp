#include "Person.h"
#include <string>

Person:Person(std::string name)
{
  this->name = name;
}

Person:~Person()
{
  
}

std::string unfriend(Person friend)
{
  boolean exists = false;
  for(int i = 0;i<friends.size();++i)
    {
      if(friends[i]->name==friend->name)
	{
	  exists = true;
	  friends.erase(friends.begin()+i);
	  return "This person was removed from your friend list succesfully";
      }
  }
  if(!exists)
    {
      return "This person does not exist in your friend list";
  }
  
}

std::string befriend(Person friend)
{
  boolean exists = false;
  for(int i = 0;i<friends.size();++i)
    {
      if(friends[i]->name==friend->name)
	{
	  exists = true;
	  return "This person is already part of your friend list";
      }
  }

  if(!exists)
    {
      friends.push_back(friend);
      return "This person was added successfully";
  }
  
}
