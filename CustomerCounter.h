#ifndef _CUSTOMERCOUNTER_H_
#define _CUSTOMERCOUNTER_H_

#include <string>

class CustomerCounter {
 private:
  int maximum_customers;
  int customer_count = 0;
 public:
  CustomerCounter(int maxNumber);
  ~CustomerCounter();
  std::string add(int number);
  std::string substract(int number);
};
